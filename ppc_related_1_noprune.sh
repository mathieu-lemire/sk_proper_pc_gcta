#!/bin/bash 


module load  plink/1.90b3x   
module load gcta/1.93.2

scriptdir=/hpf/projects/arnold/users/mlemire/scripts/proper_pc_gcta

# this should be gwas analysis-ready 
plinkprefix=$1

# this will be the grm to use in the gwas 
pcgrm=$2 

grep X $scriptdir/exclude_range.txt > $$_exclrange





plink --memory 8000 --bfile $plinkprefix --exclude range $$_exclrange  \
 --make-bed  --out $$_plink --maf 0.01 

gcta64 --bfile  $$_plink  --maf 0.01 --make-grm --out $pcgrm  --thread-num 1 

\rm $$_* 







