#!/bin/bash 


module load  plink/1.90b3x   
module load gcta/1.93.2

scriptdir=/hpf/projects/arnold/users/mlemire/scripts/proper_pc_gcta

# this should be gwas analysis-ready 
plinkprefix=$1

# this will be the grm to use in the gwas 
pcgrm=$2 

# exclude range, can be empty 
excluderange=$3


if [ -z $excluderange ]; then 
 excluderange=$scriptdir/exclude_range.txt
fi 

plink --memory 8000 --bfile $plinkprefix --exclude range $excluderange \
 --indep-pairwise 2500 500 0.1 --out  ${pcgrm}_pruned --maf 0.01 

plink --memory 8000 --bfile  $plinkprefix --extract ${pcgrm}_pruned.prune.in --make-bed \
  --out ${pcgrm}_pruned  

gcta64 --bfile ${pcgrm}_pruned --maf 0.01 --make-grm --out $pcgrm  --thread-num 1 






