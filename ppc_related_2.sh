#!/bin/bash 


module load  plink/1.90b3x   
module load gcta/1.93.2

scriptdir=/hpf/projects/arnold/users/mlemire/scripts/proper_pc_gcta

# same as in ppc_related_1.sh
plinkprefix=$1
pcgrm=$2 

# related cutoff 
cutoff=$3 
pcoutput=$4 
npc=$5 

echo CUTOFF $cutoff > cutoff.log 


gcta64 --grm $pcgrm  --grm-cutoff $cutoff  --make-grm --out $$_tmp_norel 

gcta64 --grm $pcgrm  --grm-singleton $cutoff  --out $$_tmp_single 

gcta64 --grm $$_tmp_norel  --pca $npc  --out $$_tmp_pc${npc}_norel


# pc calc among the unrelated 
plink --memory 8000 --bfile ${pcgrm}_pruned   --keep $$_tmp_norel.grm.id  --make-bed --out $$_tmp_norel

nnorel=`wc -l $$_tmp_norel.fam | awk '{print $1}'`
nsamples=`wc -l ${pcgrm}_pruned.fam | awk '{print $1}'`


# if all samples are "unrelated" (wrt cutoff) 
if [ $nnorel -eq $nsamples ]; then 

 # nothing more to be done, just write the output 
 sed 's/\t/ /g'  $$_tmp_pc${npc}_norel.eigenvec | sort > ${pcoutput}.eigenvec
 
else 

# need to project the related ones 

 plink --memory 8000 --bfile ${pcgrm}_pruned   --remove $$_tmp_norel.grm.id  --make-bed --out $$_tmp_rel

 gcta64 --bfile $$_tmp_norel  --pc-loading $$_tmp_pc${npc}_norel    --out $$_loadings 

 # projecting the related ones 
 gcta64 --bfile $$_tmp_rel  --project-loading $$_loadings $npc --out $$_tmp_pc${npc}_rel 

 # all the samples are there 
 cat $$_tmp_pc${npc}_norel.eigenvec $$_tmp_pc${npc}_rel.proj.eigenvec | sed 's/\t/ /g' | sort > ${pcoutput}.eigenvec 
 cp $$_tmp_pc${npc}_norel.eigenval ${pcoutput}.eigenval

 # to account for shrinkage of the projected ones, I will add a (PROJ)*(PC) interaction term in the model
 # where PROJ is an indicator of wether a sample was projected or not 
 # these products will be in another covariate file ${pcoutput}.proj.eigenvec

 awk '{print $1"_"$2,$0}' $$_tmp_pc${npc}_rel.proj.eigenvec | sort > $$_tmp_rel_pc 

 awk '{print $1"_"$2}' ${pcoutput}.eigenvec | sort > $$_all_samples 

 join -1 1 -2 1 -v 1 $$_all_samples $$_tmp_rel_pc |\
  awk '{printf $1}{for( i=0; i<'$npc'; i++){printf " 0"}}{printf "\n"}' | sed 's/_/ /'  >> $$_tmp_pc${npc}_rel.proj.eigenvec 

 sort $$_tmp_pc${npc}_rel.proj.eigenvec| sed 's/\t/ /g'  > ${pcoutput}.proj.eigenvec

fi 

\rm $$_* 



















