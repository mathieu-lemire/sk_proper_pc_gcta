
args<- commandArgs(TRUE)

grmprefix=args[1]

minkinship<-0.05 
if( length(args)>1 ){
 minkinship=as.numeric( commandArgs(TRUE)[2] )
}


ReadGRMBin=function(prefix, AllN=F, size=4){
  sum_i=function(i){
    return(sum(1:i))
  }
  BinFileName=paste(prefix,".grm.bin",sep="")
  NFileName=paste(prefix,".grm.N.bin",sep="")
  IDFileName=paste(prefix,".grm.id",sep="")
  id = read.table(IDFileName)
  n=dim(id)[1]
  BinFile=file(BinFileName, "rb");
  grm=readBin(BinFile, n=n*(n+1)/2, what=numeric(0), size=size)
  NFile=file(NFileName, "rb");
  if(AllN==T){
    N=readBin(NFile, n=n*(n+1)/2, what=numeric(0), size=size)
  }
  else N=readBin(NFile, n=1, what=numeric(0), size=size)
  i=sapply(1:n, sum_i)
  return(list(diag=grm[i], off=grm[-i], id=id, N=N))
}

cmb<- ReadGRMBin(grmprefix) 
k<-cmb$off ; k<-k[k> minkinship ] 


pdf( "_cutoff.pdf", height=6, width=12 )
par( mfrow=c(1,2) )
plot(   sort(k) , ylab="2*kinship" , xlab="sample (sorted)" ) 
abline( h=seq(0.05,1,.05 ) )

plot(  sort(k)  , ylim=c( minkinship ,.3 )  , ylab="2*kinship (zoomed)" , xlab="sample (sorted)" ) 
abline( h=seq(0.05,.3,.01 ) )
dev.off()



